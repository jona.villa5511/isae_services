package com.isae.inventario.dao.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	private JavaMailSender sender;

	//@Override
	public void sendEmail(EmailBody emailBody)  {
		System.out.println("EmailBody: {} "+ emailBody.toString());
		 sendEmailTool(emailBody.getContent(),emailBody.getEmail(), emailBody.getSubject());
	}
	

	private void sendEmailTool(String textMessage, String email,String subject) {
		//boolean send = false;
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);		
		try {
			helper.setTo(email);
			helper.setText(textMessage, true);
			helper.setSubject(subject);
			sender.send(message);
			//send = true;
			System.out.println("Mail enviado!");
		} catch (MessagingException e) {
			System.err.println("Hubo un error al enviar el mail: {}" +e);
		}
		
	}

	
	
}
