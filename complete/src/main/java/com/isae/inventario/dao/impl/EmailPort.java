package com.isae.inventario.dao.impl;

public interface EmailPort {
	public boolean sendEmail(EmailBody emailBody);
}
