package com.isae.inventario.dao.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ConexionBD {

	private final Logger logger = LoggerFactory.getLogger(ConexionBD.class);

	public CallableStatement callableStatement;
	public Connection connection;
	public ResultSet resultSet;
	
	

	public Connection getConnectionBD() {
		System.out.println("-------------- Conectando base driver ....... ------------------");

		try {
			connection = null;
			resultSet = null;
			callableStatement = null;

			Class.forName(Constants._BD_DRIVER);
			connection = DriverManager.getConnection(Constants._BD_URL,	Constants._BD_USR, Constants._BD_PSW);
			System.out.println("-------------- Conexion Correcta :) ------------------");

		} catch (SQLException |ClassNotFoundException ex) {

			logger.error("METHOD[getConnectionBD()]"+
					ex.getMessage());
		}
		return connection;
	}

	public void closeResources(ResultSet resultSet, CallableStatement callableStatement, Connection connection) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (callableStatement != null) {
				callableStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException ex) {
			logger.error("Class[ConexionBD], METHOD[closeResources()], EXCEPTION[{}], DATE[{}]::#"+
					ex.getMessage());
		}

	}

//	public String parseBase64(String string) {
//		String stringDecoded = "";
//
//		try {
//			byte[] authBuffer = DatatypeConverter.parseBase64Binary(string);
//			stringDecoded = new String(authBuffer, Constants.UTF8);
//
//		} catch (Exception ex) {
//
//			logger.error("Class[ConexionBD], METHOD[parseBase64()], EXCEPTION[{}], DATE[{}]::#",
//					ex.getMessage(), Constants.getLoggerDate());
//		}
//		return stringDecoded.trim();
//	}

}
