package com.isae.inventario.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isae.inventario.dao.util.ConexionBD;
import com.isae.inventario.dao.util.Constants;
import com.isae.inventario.modelo.Agrupador;
import com.isae.inventario.modelo.Auxiliar;
import com.isae.inventario.modelo.DetalleAgrupador;
import com.isae.inventario.modelo.Files;
import com.isae.inventario.modelo.Inventario;
import com.isae.inventario.modelo.Proyect;
import com.isae.inventario.modelo.User;



public class CamposAgrupadorDaoImpl extends ConexionBD implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Logger logger = LoggerFactory.getLogger(CamposAgrupadorDaoImpl.class);

	
	public List<Proyect> getProyects(int opcion) {
		List<Proyect> ListProyect = new ArrayList<Proyect>();
		System.out.println("getProyects:::Dao::");
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.SP_GetProyects);
			callableStatement.setInt(1, opcion);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				ListProyect.add(new Proyect(resultSet.getInt(1), resultSet.getString(2),resultSet.getString(3)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getProyects()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao::getProyects " + ListProyect.size());
		return ListProyect;
	}
	
	
	public List<Inventario> getInventario(Integer idProyec,Integer idUsuario) {
		List<Inventario> ListInvent = new ArrayList<Inventario>();
		System.out.println("getInventario:::Dao:::IdProyect "+ idProyec );
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.SP_GetInventario);
			callableStatement.setInt(1, idProyec);
			callableStatement.setInt(2, idUsuario);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				ListInvent.add(new Inventario(resultSet.getInt(1), resultSet.getString(6),resultSet.getString(186)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getInventario()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao::getInventario " + ListInvent.size());
		return ListInvent;
	}
	
	public List<Inventario> getInventarioBuscar(String folio, Integer idProyec,Integer idUsuario) {
		List<Inventario> ListInvent = new ArrayList<Inventario>();
		System.out.println("getInventario:::Dao:::IdProyect "+ idProyec );
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.SP_GetInventarioBuscar);

			callableStatement.setString(1, folio);
			callableStatement.setInt(2, idProyec);
			callableStatement.setInt(3, idUsuario);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				ListInvent.add(new Inventario(resultSet.getInt(1), resultSet.getString(6),resultSet.getString(186)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getInventarioBuscar()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao::getInventario " + ListInvent.size());
		return ListInvent;
	}
	public String getMaxInventario(Integer idProyec) {
		String max =  "";
		System.out.println("getInventario:::Dao:::IdProyect "+ idProyec );
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.SP_GetMaxInventario);
			callableStatement.setInt(1, idProyec);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				max = String.valueOf(resultSet.getInt(1));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getMaxInventario()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao::getMaxInventario " + max);
		return max;
	}
	
	
	public List<Inventario> getProyectBuscar(String descripcion) {
		List<Inventario> ListInvent = new ArrayList<Inventario>();
		System.out.println("getProyectBuscar:::Dao:::descripcion "+ descripcion );
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.SP_GetProyectBuscar);

			callableStatement.setString(1, descripcion);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				ListInvent.add(new Inventario(resultSet.getInt(1), resultSet.getString(6),resultSet.getString(186)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getProyectBuscar()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao::getInventario " + ListInvent.size());
		return ListInvent;
	}
	
	
	public List<DetalleAgrupador> getDetalleAgrupador(Integer idProyec, Integer idInven, Integer idAgrup, Integer idUsuario) {
		List<DetalleAgrupador> detalle = new ArrayList<DetalleAgrupador>();
		System.out.println("paramerrtso Dao::: "+ idProyec + " " + idInven + " " + idAgrup);
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.UnPivotInventario2);
			callableStatement.setInt(1, idProyec);
			callableStatement.setInt(2, idInven);
			callableStatement.setInt(3, idAgrup);
			callableStatement.setInt(4, idUsuario);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				detalle.add(new DetalleAgrupador(resultSet.getInt(10), resultSet.getString(19),resultSet.getString(20),resultSet.getString(4),resultSet.getString(2),resultSet.getString(15),resultSet.getString(16)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getDetalleAgrupador()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao:: " + detalle);
		return detalle;
	}
	
	public List<DetalleAgrupador> getDetalleAgrupadorADD(Integer idProyec, Integer idAgrup, Integer idUsuario) {
		List<DetalleAgrupador> detalle = new ArrayList<DetalleAgrupador>();
		System.out.println("paramerrtso Dao::: "+ idProyec  + " " + idAgrup);
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.UnPivotPROYECTO);
			callableStatement.setInt(1, idProyec);
			callableStatement.setInt(2, idAgrup);
			callableStatement.setInt(3, idUsuario);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				detalle.add(new DetalleAgrupador(resultSet.getInt(10), resultSet.getString(19),resultSet.getString(20),resultSet.getString(4),resultSet.getString(2),resultSet.getString(15),resultSet.getString(16)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getDetalleAgrupadorADD()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDao:: " + detalle);
		return detalle;
	}

	public List<Agrupador> getAgrupador(Integer idProyec, Integer idUsuario) {
		List<Agrupador> agrup = new ArrayList<Agrupador>();
		
		try {
			connection = getConnectionBD();

			callableStatement = connection.prepareCall(Constants.sp_AgrupacionUsuario);
			callableStatement.setInt(1, idProyec);
			callableStatement.setInt(2, idUsuario);
			callableStatement.execute();
			
			resultSet = callableStatement.getResultSet();
	
			while (resultSet.next()) {
				agrup.add(new Agrupador(resultSet.getInt(1),resultSet.getString(2)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getAgrupador()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		System.out.println("listaDaoAgrupador:: " + agrup.size());
		return agrup;
	}

	
	public void updateInventario(Integer idProyec, Integer idInven, String campo, String valor) {
		
		try {
			connection = getConnectionBD();
			callableStatement = connection.prepareCall(Constants.SP_UpdateInventario);
			callableStatement.setInt(1, idProyec);
			callableStatement.setInt(2, idInven);
			callableStatement.setString(3, campo);
			callableStatement.setString(4, valor);
			callableStatement.executeUpdate();
			System.out.println("Update::  se actualizo el campo:. " + campo);
			

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[updateInventario()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		
		
	}
	
public List<User> getUserLogin(String user, String pass) {
	List<User> Listuser = new ArrayList<User>();
	
		try {
			connection = getConnectionBD();
			callableStatement = connection.prepareCall(Constants.SP_GetUserLogin);
			callableStatement.setInt(1, 1);
			callableStatement.setString(2, user);
			callableStatement.setString(3, pass);
			callableStatement.executeUpdate();
			System.out.println("User:: " + user + " Pass::"+pass);
			resultSet = callableStatement.getResultSet();
			
			while (resultSet.next()) {
				Listuser.add(new User(resultSet.getInt(1),resultSet.getString(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getString(5),resultSet.getString(6),resultSet.getInt(7),resultSet.getString(8)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getUserLogin()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		
		return Listuser;
	}
public List<User> getUser(String user) {
	List<User> Listuser = new ArrayList<User>();
	
		try {
			connection = getConnectionBD();
			callableStatement = connection.prepareCall(Constants.SP_GetUserLogin);
			callableStatement.setInt(1, 2);
			callableStatement.setString(2, user);
			callableStatement.setString(3, null);
			callableStatement.executeUpdate();
			System.out.println("User:: " + user);
			resultSet = callableStatement.getResultSet();
			
			while (resultSet.next()) {
				Listuser.add(new User(resultSet.getInt(1),resultSet.getString(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getString(5),resultSet.getString(6),resultSet.getInt(7),resultSet.getString(8)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getUser()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		
		return Listuser;
	}
public List<User> getUserCorreo(String user) {
	List<User> Listuser = new ArrayList<User>();
	
		try {
			connection = getConnectionBD();
			callableStatement = connection.prepareCall(Constants.SP_GetUserLogin);
			callableStatement.setInt(1, 3);
			callableStatement.setString(2, user);
			callableStatement.setString(3, null);
			callableStatement.executeUpdate();
			System.out.println("User:: " + user);
			resultSet = callableStatement.getResultSet();
			
			while (resultSet.next()) {
				Listuser.add(new User(resultSet.getInt(1),resultSet.getString(2),resultSet.getInt(3),resultSet.getInt(4),resultSet.getString(5),resultSet.getString(6),resultSet.getInt(7),resultSet.getString(8)));
			}

		} catch (Exception ex) {
			logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getUser()];  EXCEPTION[" + ex.getMessage()
					+ "]; ");
		} finally {
			closeResources(resultSet, callableStatement, connection);
		}
		
		return Listuser;
	}

public void updateUser(Integer idUser, String newPass) {
	
	try {
		connection = getConnectionBD();
		callableStatement = connection.prepareCall(Constants.SP_UpdateUserLogin);
		callableStatement.setInt(1, idUser);
		callableStatement.setString(2, newPass);
		callableStatement.executeUpdate();
		System.out.println("Update::  se actualizo la contraseña :. " + idUser);
		

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[updateUser()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	
	
}




public List<Auxiliar> getCatalogos(Integer id) {
	List<Auxiliar> listAux = new ArrayList<Auxiliar>();
	System.out.println("getEstados:::Dao:::CodigoPostal "+ id );
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_GetCatalogo);
		callableStatement.setInt(1, id);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			listAux.add(new Auxiliar(resultSet.getInt(1), resultSet.getString(2)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getCatalogos()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("listaDao::getCatalogos " + listAux.size());
	return listAux;
}

public Integer insertInventario(Inventario invent) {
	System.out.println("DAO INVENTARIO::: " + invent.getUsuario());
	
	Auxiliar aux = null ;
	try {
		connection = getConnectionBD();
		callableStatement = connection.prepareCall(Constants.SP_InsertInventario);
		callableStatement.setInt(1, 0);
		callableStatement.setLong(2, invent.getProyectoid());
		callableStatement.setString(3, invent.getProyecto());
		callableStatement.setString(4, invent.getProyectodescripcion());
		callableStatement.setString(5, invent.getFcreacon());
		callableStatement.setString(6, invent.getFolio());
		callableStatement.setLong(7, invent.getId());
		callableStatement.setString(8, invent.getaPaterno());
		callableStatement.setString(9, invent.getaMaterno());
		callableStatement.setString(10, invent.getNombres());
		callableStatement.setString(11, invent.getNombrecompleto());
		callableStatement.setLong(12, invent.getNumempleado());
		callableStatement.setString(13, invent.getVip());
		callableStatement.setString(14, invent.getPuesto());
		callableStatement.setString(15, invent.getDireccion());
		callableStatement.setString(16, invent.getSubdireccion());
		callableStatement.setLong(17, invent.getClavesubdireccion());
		callableStatement.setString(18, invent.getGerencia());
		callableStatement.setLong(19, invent.getClavegerencia());
		callableStatement.setString(20, invent.getDepto());
		callableStatement.setLong(21, invent.getClavecentrotrabajo());
		callableStatement.setString(22, invent.getCorreo());
		callableStatement.setString(23, invent.getTelefono());
		callableStatement.setLong(24, invent.getExt());
		callableStatement.setString(25, invent.getUbicacion());
		callableStatement.setLong(26, invent.getCp());
		callableStatement.setString(27, invent.getEstado());
		callableStatement.setString(28, invent.getColonia());
		callableStatement.setString(29, invent.getUbicacioncompleta());
		callableStatement.setString(30, invent.getZona());
		callableStatement.setString(31, invent.getLocalidad());
		callableStatement.setString(32, invent.getEdificio());
		callableStatement.setString(33, invent.getPiso());
		callableStatement.setString(34, invent.getArea());
		callableStatement.setString(35, invent.getAdscripcion());
		callableStatement.setString(36, invent.getApellidosjefe());
		callableStatement.setString(37, invent.getApellidos2jefe());
		callableStatement.setString(38, invent.getNombresjefe());
		callableStatement.setString(39, invent.getNombrecompletojefe());
		callableStatement.setLong(40, invent.getFichajefe());
		callableStatement.setLong(41, invent.getExtjefe());
		callableStatement.setString(42, invent.getUbicacionjefe());
		callableStatement.setString(43, invent.getNombrejefeinmediato());
		callableStatement.setString(44, invent.getApellidosresguardo());
		callableStatement.setString(45, invent.getApellidos2resguardo());
		callableStatement.setString(46, invent.getNombresresguardo());
		callableStatement.setString(47, invent.getNombrecompletoresguardo());
		callableStatement.setString(48, invent.getAdscripcionresguardo());
		callableStatement.setLong(49, invent.getExtresguardo());
		callableStatement.setString(50, invent.getApellidosresponsable());
		callableStatement.setString(51, invent.getApellidos2responsable());
		callableStatement.setString(52, invent.getNombresresponsable());
		callableStatement.setString(53, invent.getNombrecompletoresponsable());
		callableStatement.setString(54, invent.getApellidospemex());
		callableStatement.setString(55, invent.getApellidos2pemex());
		callableStatement.setString(56, invent.getNombrespemex());
		callableStatement.setString(57, invent.getNombrecompletopemex());
		callableStatement.setString(58, invent.getTipoequipo());
		callableStatement.setString(59, invent.getEquipo());
		callableStatement.setString(60, invent.getMarcaequipo());
		callableStatement.setString(61, invent.getModeloequipo());
		callableStatement.setString(62, invent.getNumserieequipo());
		callableStatement.setString(63,invent.getEquipocompleto());
		callableStatement.setString(64,invent.getMonitor());
		callableStatement.setString(65,invent.getMarcamonitor());
		callableStatement.setString(66,invent.getModelomonitor());
		callableStatement.setString(67,invent.getNumseriemonitor());
		callableStatement.setString(68,invent.getMonitorcompleto());
		callableStatement.setString(69,invent.getTeclado());
		callableStatement.setString(70,invent.getMarcateclado());
		callableStatement.setString(71,invent.getModeloteclado());
		callableStatement.setString(72,invent.getNumserieteclado());
		callableStatement.setString(73,invent.getTecladocompleto());
		callableStatement.setString(74,invent.getMouse());
		callableStatement.setString(75,invent.getMarcamouse());
		callableStatement.setString(76,invent.getModelomause());
		callableStatement.setString(77,invent.getNumseriemouse());
		callableStatement.setString(78,invent.getMousecompleto());
		callableStatement.setString(79,invent.getUps());
		callableStatement.setString(80,invent.getMarcaups());
		callableStatement.setString(81,invent.getModeloups());
		callableStatement.setString(82,invent.getNumserieups());
		callableStatement.setString(83,invent.getUpscompleto());
		callableStatement.setString(84,invent.getMaletin());
		callableStatement.setString(85,invent.getMarcamaletin());
		callableStatement.setString(86,invent.getModelomaletin());
		callableStatement.setString(87,invent.getNumseriemaletin());
		callableStatement.setString(88,invent.getMaletincomleto());
		callableStatement.setString(89,invent.getCandado());
		callableStatement.setString(90,invent.getMarcacandado());
		callableStatement.setString(91,invent.getModelocandado());
		callableStatement.setString(92,invent.getNumseriecandado());
		callableStatement.setString(93,invent.getCandadocompleto());
		callableStatement.setString(94,invent.getBocinas());
		callableStatement.setString(95,invent.getMarcabocinas());
		callableStatement.setString(96,invent.getModelobocinas());
		callableStatement.setString(97,invent.getNumseriebocinas());
		callableStatement.setString(98,invent.getBocinascompleto());
		callableStatement.setString(99,invent.getCamara());
		callableStatement.setString(100,invent.getMarcacamara());
		callableStatement.setString(101,invent.getModelocamara());
		callableStatement.setString(102,invent.getNumseriecmara());
		callableStatement.setString(103,invent.getCamaracompleto());
		callableStatement.setString(104, invent.getMonitor2());
		callableStatement.setString(105, invent.getMarcamonitor2());
		callableStatement.setString(106, invent.getModelomonitor2());
		callableStatement.setString(107, invent.getNumseriemonitor2());
		callableStatement.setString(108, invent.getMonitor2completo());
		callableStatement.setString(109, invent.getAccesorio());
		callableStatement.setString(110, invent.getMarcaaccesorio());
		callableStatement.setString(111, invent.getModeloaccesorio());
		callableStatement.setString(112, invent.getNumserieaccesorio());
		callableStatement.setString(113, invent.getAccesoriocompleto());
		callableStatement.setString(114, invent.getRam());
		callableStatement.setString(115, invent.getDiscoduro());
		callableStatement.setString(116, invent.getProcesador());
		callableStatement.setString(117, invent.getTipoequipocomp1());
		callableStatement.setString(118, invent.getModelocomp1());
		callableStatement.setString(119, invent.getNumseriecomp1());
		callableStatement.setString(120, invent.getCruceclientecomp1());
		callableStatement.setString(121, invent.getTipoequipocomp2());
		callableStatement.setString(122, invent.getModelocomp2());
		callableStatement.setString(123, invent.getNumseriecomp2());
		callableStatement.setString(124, invent.getCruceclientecomp2());
		callableStatement.setString(125, invent.getTipoequipocomp3());
		callableStatement.setString(126, invent.getModelocomp3());
		callableStatement.setString(127, invent.getNumseriecomp3());
		callableStatement.setString(128, invent.getCruceclientecomp3());
		callableStatement.setString(129, invent.getTipoequipocomp4());
		callableStatement.setString(130, invent.getModelocomp4());
		callableStatement.setString(131, invent.getNumseriecomp4());
		callableStatement.setString(132, invent.getCruceclientecomp4());
		callableStatement.setString(133, invent.getTipoequipocomp5());
		callableStatement.setString(134, invent.getModelocomp5());
		callableStatement.setString(135, invent.getNumseriecomp5());
		callableStatement.setString(136, invent.getCruceclientecomp5());
		callableStatement.setString(137, invent.getTipoequipocomp6());
		callableStatement.setString(138, invent.getModelocomp6());
		callableStatement.setString(139, invent.getNumseriecomp6());
		callableStatement.setString(140, invent.getCruceclientecomp6());
		callableStatement.setString(141, invent.getTipoequipocomp7());
		callableStatement.setString(142, invent.getModelocomp7());
		callableStatement.setString(143, invent.getNumseriecomp7());
		callableStatement.setString(144, invent.getCruceclientecomp7());
		callableStatement.setString(145, invent.getValidacioncomp1());
		callableStatement.setString(146, invent.getValidacioncomp2());
		callableStatement.setString(147, invent.getValidacioncomp3());
		callableStatement.setString(148, invent.getValidacioncomp4());
		callableStatement.setString(149, invent.getValidacioncomp5());
		callableStatement.setString(150, invent.getValidacioncomp6());
		callableStatement.setString(151, invent.getValidacioncomp7());
		callableStatement.setLong(152, invent.getValidadoscomp());
		callableStatement.setString(153, invent.getTecniconombre());
		callableStatement.setString(154, invent.getDia());
		callableStatement.setString(155, invent.getMes());
		callableStatement.setString(156, invent.getAnio());
		callableStatement.setString(157, invent.getReqespecial1());
		callableStatement.setString(158, invent.getReqespecial2());
		callableStatement.setString(159, invent.getObsinv());
		callableStatement.setString(160, invent.getObsresguardo());
		callableStatement.setString(161, invent.getObsextras1());
		callableStatement.setString(162, invent.getObsextras2());
		callableStatement.setString(163, invent.getEstatus());
		callableStatement.setString(164, invent.getFescalacion());
		callableStatement.setString(165, invent.getComentariosescalacion());
		callableStatement.setString(166, invent.getCampolibre1());
		callableStatement.setString(167, invent.getCampolibre2());
		callableStatement.setString(168, invent.getCampolibre3());
		callableStatement.setString(169, invent.getCampolibre4());
		callableStatement.setString(170, invent.getCampolibre5());
		callableStatement.setString(171, invent.getCampolibre6());
		callableStatement.setString(172, invent.getCampolibre7());
		callableStatement.setString(173, invent.getCampolibre8());
		callableStatement.setString(174, invent.getCampolibre9());
		callableStatement.setString(175, invent.getCampolibre10());
		callableStatement.setString(176, invent.getCampolibre11());
		callableStatement.setString(177, invent.getCampolibre12());
		callableStatement.setString(178, invent.getCampolibre13());
		callableStatement.setString(179, invent.getCampolibre14());
		callableStatement.setString(180, invent.getCampolibre15());
		callableStatement.setString(181, invent.getCampolibre16());
		callableStatement.setString(182, invent.getCampolibre17());
		callableStatement.setString(183, invent.getCampolibre18());
		callableStatement.setString(184, invent.getCampolibre19());
		callableStatement.setString(185, invent.getCampolibre20());
		callableStatement.setString(186, invent.getCampoId());
		callableStatement.setString(187, invent.getUsuario());
		
		
		callableStatement.executeUpdate();
		System.out.println("Insert::  se agrego el inventario:. ");
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			aux = new Auxiliar(resultSet.getInt(1));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[insertInventario()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("Id nuevo del insert inventario "+aux.getId());
	return aux.getId();
	
}

public List<Auxiliar> getEstado() {
	List<Auxiliar> listAux = new ArrayList<Auxiliar>();
	
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getEstados);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			listAux.add(new Auxiliar(resultSet.getString(1)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getEstado()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("getEstados:: " + listAux.size());
	return listAux;
}
public List<Auxiliar> getLocalidad(String descripcion) {
	List<Auxiliar> listAux = new ArrayList<Auxiliar>();
	
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getLocalidad);
		callableStatement.setString(1, descripcion);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			listAux.add(new Auxiliar(resultSet.getString(1)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getLocalidad()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("getLocalidades:: " + listAux.size());
	return listAux;
}

public List<Auxiliar> getColonia(String descripcion) {
	List<Auxiliar> listAux = new ArrayList<Auxiliar>();
	
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getColonia);
		callableStatement.setString(1, descripcion);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			listAux.add(new Auxiliar(resultSet.getString(1)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getColonia()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("getColonias:: " + listAux.size());
	return listAux;
}

public List<Auxiliar> getAdressCP(String cp) {
	List<Auxiliar> listAux = new ArrayList<Auxiliar>();
	
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getAdressCP);
		callableStatement.setString(1, cp);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			listAux.add(new Auxiliar(resultSet.getString(1),resultSet.getString(2),resultSet.getString(3)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getAdressCP()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("getAdressCP:: " + listAux.size());
	return listAux;
}


public List<Auxiliar> getCatMarcaModelo(Integer idCat, String marca) {
	List<Auxiliar> listAux = new ArrayList<Auxiliar>();
	
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_GetCatalogoMarcaModelo);
		callableStatement.setInt(1, idCat);
		callableStatement.setString(2, marca);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			listAux.add(new Auxiliar(0,resultSet.getString(1)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getCatMarcaModelo()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("getCatMarcaModelo:: " + listAux.size());
	return listAux;
}

public List<Files> getFiles(Integer idProyec, Integer idInven) {
	List<Files> files = new ArrayList<Files>();
	System.out.println("getFiles Dao::: "+ idProyec + " " + idInven );
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getFiles);
		callableStatement.setInt(1, idInven);
		callableStatement.setInt(2, idProyec);
		callableStatement.setInt(3, 1);
		callableStatement.setString(4, "");
		
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			files.add(new Files(resultSet.getInt(1),
					resultSet.getString(2),
					resultSet.getString(3),
					resultSet.getString(4),
					resultSet.getInt(5),
					resultSet.getString(6),
					resultSet.getInt(7),
					resultSet.getInt(8),
					"0"
					
				));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getFiles()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("listaDao:: " + files);
	return files;
}
public List<Files> getFilesByNumeroSerie(Integer idProyec, Integer idInven, String fileName) {
	List<Files> files = new ArrayList<Files>();
	System.out.println("getFiles Dao::: "+ idProyec + " " + idInven );
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getFiles);
		callableStatement.setInt(1, idInven);
		callableStatement.setInt(2, idProyec);
		callableStatement.setInt(3, 3);
		callableStatement.setString(4, fileName);
		
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			files.add(new Files(resultSet.getInt(1),
					resultSet.getString(2),
					resultSet.getString(3),
					resultSet.getString(4),
					resultSet.getInt(5),
					resultSet.getString(6),
					resultSet.getInt(7),
					resultSet.getInt(8),
					"0"
					
				));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getFilesByNumeroSerie()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("listaDao:: " + files);
	return files;
}

public List<Files> getFilesValidate(Integer idProyec, Integer idInven, String nameFile) {
	List<Files> files = new ArrayList<Files>();
	System.out.println("getFiles Dao::: "+ idProyec + " " + idInven );
	try {
		connection = getConnectionBD();

		callableStatement = connection.prepareCall(Constants.SP_getFiles);
		callableStatement.setInt(1, idInven);
		callableStatement.setInt(2, idProyec);
		callableStatement.setInt(3, 2);
		callableStatement.setString(4, nameFile);
		callableStatement.execute();
		
		resultSet = callableStatement.getResultSet();

		while (resultSet.next()) {
			files.add(new Files(resultSet.getInt(1)));
		}

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[getFilesValidate()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	System.out.println("listaDao:: " + files);
	return files;
}

public void insertFiles(Files files) {
	
	try {
		connection = getConnectionBD();
		callableStatement = connection.prepareCall(Constants.SP_InsertaFileInventario);
		callableStatement.setString(1, files.getFileName());
		callableStatement.setString(2, files.getFileUrl());
		callableStatement.setString(3, files.getFileCoordenadas());
		callableStatement.setInt(4, files.getIdUsuario());
		callableStatement.setString(5, null);
		callableStatement.setInt(6, files.getIdInventario());
		callableStatement.setInt(7, files.getIdProyecto());
		callableStatement.setString(8, files.getTipo());
		callableStatement.executeUpdate();
		System.out.println("Inserta Files url :. " + files.getFileUrl());
		

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[insertFiles()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	
	
}
public void insertFilesOtros(Files files) {
	
	try {
		connection = getConnectionBD();
		callableStatement = connection.prepareCall(Constants.SP_InsertaFileOtrosInventario);
		callableStatement.setString(1, files.getFileName());
		callableStatement.setString(2, files.getFileUrl());
		callableStatement.setString(3, files.getFileCoordenadas());
		callableStatement.setInt(4, files.getIdUsuario());
		callableStatement.setString(5, null);
		callableStatement.setInt(6, files.getIdInventario());
		callableStatement.setInt(7, files.getIdProyecto());
		callableStatement.setString(8, files.getTipo());
		callableStatement.executeUpdate();
		System.out.println("Inserta Files url :. " + files.getFileUrl());
		

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[insertFilesOtros()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	
	
}
public void deleteFiles(Files files) {
	
	try {
		connection = getConnectionBD();
		callableStatement = connection.prepareCall(Constants.SP_DeleteFiles);
		callableStatement.setInt(1, files.getId());
		callableStatement.executeUpdate();
		System.out.println("eleimina Files url :. " + files.getId());
		

	} catch (Exception ex) {
		logger.error("{};ISAE:Class[CamposAgrupadorDaoImpl]; METHOD[deleteFiles()];  EXCEPTION[" + ex.getMessage()
				+ "]; ");
	} finally {
		closeResources(resultSet, callableStatement, connection);
	}
	
	
}
public  String enviarGmail(Integer opcion) {
	String retornoMensaje="";
//	try {
		
//	    Properties props = new Properties();
//	    props.setProperty("mail.smtp.host", "smtp.gmail.com");
//	    props.setProperty("mail.smtp.starttls.enable", "true");
//	    props.setProperty("mail.smtp.port", "587");
//	    props.setProperty("mail.smtp.auth", "true");
//
//	    Session session = Session.getDefaultInstance(props);
//
//	    String correoRemitente = "jona.villa5511@gmail.com";
//	    String passwordRemitente = "jona5511";
//	    String correoReceptor = "jona_5511@hotmail.com";
//	    String asunto = "Mi primero correo en Java";
//	    String mensaje = "Hola<br>Este es el contenido de mi primer correo desde <b>java</b><br><br>Por <b>Códigos de Programación</b>";
//
//	    BodyPart texto = new MimeBodyPart();
//	    texto.setContent(mensaje, "text/html");
//
////	    BodyPart adjunto = new MimeBodyPart();
////	    adjunto.setDataHandler(new DataHandler(new FileDataSource("d:/Imagen1.png")));
////	    adjunto.setFileName("Image.png");
//
//	    MimeMultipart miltiParte = new MimeMultipart();
//	    miltiParte.addBodyPart(texto);
//	    //miltiParte.addBodyPart(adjunto);
//
//	    MimeMessage message = new MimeMessage(session);
//	    message.setFrom(new InternetAddress(correoRemitente));
//
//	    message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoReceptor));
//	    message.setSubject(asunto);
//	    message.setContent(miltiParte);
//
//	    Transport t = session.getTransport("smtp");
//	    t.connect(correoRemitente, passwordRemitente);
//	    t.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
//	    t.close();
//System.out.println("envio correo..");
retornoMensaje = "envio correo correctamente";

//	} catch (AddressException ex) {
//	   System.out.println(ex.getMessage());
//	} catch (MessagingException ex) {
//		 System.out.println(ex.getMessage());
//	}
	return retornoMensaje;
}

}
