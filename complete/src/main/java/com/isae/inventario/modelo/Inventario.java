package com.isae.inventario.modelo;

public class Inventario {

	public Inventario() {

	}

	public Inventario(long inventarioid, String folio,String campoId) {
		this.inventarioid = inventarioid;
		this.folio = folio;
		this.campoId=campoId;
		
		
	}

	private long inventarioid;

	private long proyectoid;

	private String proyecto;

	private String proyectodescripcion;

	private String fcreacon;

	private String folio;

	private long id;

	private String aPaterno;

	private String aMaterno;

	private String nombres;

	private String nombrecompleto;

	private long numempleado;

	private String vip;

	private String puesto;

	private String direccion;

	private String subdireccion;

	private long clavesubdireccion;

	private String gerencia;

	private long clavegerencia;

	private String depto;

	private long clavecentrotrabajo;

	private String correo;

	private String telefono;

	private long ext;

	private String ubicacion;

	private String colonia;

	private long cp;

	private String estado;

	private String ubicacioncompleta;

	private String zona;

	private String localidad;

	private String edificio;

	private String piso;

	private String area;

	private String adscripcion;

	private String apellidosjefe;
	private String apellidos2jefe;

	private String nombresjefe;

	private String nombrecompletojefe;

	private long fichajefe;

	private long extjefe;

	private String ubicacionjefe;

	private String nombrejefeinmediato;

	private String apellidosresguardo;
	private String apellidos2resguardo;

	private String nombresresguardo;

	private String nombrecompletoresguardo;

	private String adscripcionresguardo;

	private long extresguardo;

	private String apellidosresponsable;
	private String apellidos2responsable;

	private String nombresresponsable;

	private String nombrecompletoresponsable;

	private String apellidospemex;
	private String apellidos2pemex;

	private String nombrespemex;

	private String nombrecompletopemex;

	private String tipoequipo;

	private String equipo;

	private String marcaequipo;

	private String modeloequipo;

	private String numserieequipo;

	private String equipocompleto;

	private String monitor;

	private String marcamonitor;

	private String modelomonitor;

	private String numseriemonitor;

	private String monitorcompleto;

	private String teclado;

	private String marcateclado;

	private String modeloteclado;

	private String numserieteclado;

	private String tecladocompleto;

	private String mouse;

	private String marcamouse;

	private String modelomause;

	private String numseriemouse;

	private String mousecompleto;

	private String ups;

	private String marcaups;

	private String modeloups;

	private String numserieups;

	private String upscompleto;

	private String maletin;

	private String marcamaletin;

	private String modelomaletin;

	private String numseriemaletin;

	private String maletincomleto;

	private String candado;

	private String marcacandado;

	private String modelocandado;

	private String numseriecandado;

	private String candadocompleto;

	private String bocinas;

	private String marcabocinas;

	private String modelobocinas;

	private String numseriebocinas;

	private String bocinascompleto;

	private String camara;

	private String marcacamara;

	private String modelocamara;

	private String numseriecmara;

	private String camaracompleto;

	private String monitor2;

	private String marcamonitor2;

	private String modelomonitor2;

	private String numseriemonitor2;

	private String monitor2completo;

	private String accesorio;

	private String marcaaccesorio;

	private String modeloaccesorio;

	private String numserieaccesorio;

	private String accesoriocompleto;

	private String ram;

	private String discoduro;

	private String procesador;

	private String tipoequipocomp1;

	private String modelocomp1;

	private String numseriecomp1;

	private String cruceclientecomp1;

	private String tipoequipocomp2;

	private String modelocomp2;

	private String numseriecomp2;

	private String cruceclientecomp2;

	private String tipoequipocomp3;

	private String modelocomp3;

	private String numseriecomp3;

	private String cruceclientecomp3;

	private String tipoequipocomp4;

	private String modelocomp4;

	private String numseriecomp4;

	private String cruceclientecomp4;

	private String tipoequipocomp5;

	private String modelocomp5;

	private String numseriecomp5;

	private String cruceclientecomp5;

	private String tipoequipocomp6;

	private String modelocomp6;

	private String numseriecomp6;

	private String cruceclientecomp6;

	private String tipoequipocomp7;

	private String modelocomp7;

	private String numseriecomp7;

	private String cruceclientecomp7;

	private String validacioncomp1;

	private String validacioncomp2;

	private String validacioncomp3;

	private String validacioncomp4;

	private String validacioncomp5;

	private String validacioncomp6;

	private String validacioncomp7;

	private long validadoscomp;

	private String tecniconombre;

	private String dia;

	private String mes;

	private String anio;

	private String reqespecial1;

	private String reqespecial2;

	private String obsinv;

	private String obsresguardo;

	private String obsextras1;

	private String obsextras2;

	private String estatus;

	private String fescalacion;

	private String comentariosescalacion;

	private String campolibre1;
	private String campolibre2;
	private String campolibre3;
	private String campolibre4;
	private String campolibre5;
	private String campolibre6;
	private String campolibre7;
	private String campolibre8;
	private String campolibre9;
	private String campolibre10;
	private String campolibre11;
	private String campolibre12;
	private String campolibre13;
	private String campolibre14;
	private String campolibre15;
	private String campolibre16;
	private String campolibre17;
	private String campolibre18;
	private String campolibre19;
	private String campolibre20;
	private String campoId;
	private String usuario;

	public long getInventarioid() {
		return inventarioid;
	}

	public void setInventarioid(long inventarioid) {
		this.inventarioid = inventarioid;
	}

	public long getProyectoid() {
		return proyectoid;
	}

	public void setProyectoid(long proyectoid) {
		this.proyectoid = proyectoid;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public String getProyectodescripcion() {
		return proyectodescripcion;
	}

	public void setProyectodescripcion(String proyectodescripcion) {
		this.proyectodescripcion = proyectodescripcion;
	}

	public String getFcreacon() {
		return fcreacon;
	}

	public void setFcreacon(String fcreacon) {
		this.fcreacon = fcreacon;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getaPaterno() {
		return aPaterno;
	}

	public void setaPaterno(String aPaterno) {
		this.aPaterno = aPaterno;
	}

	public String getaMaterno() {
		return aMaterno;
	}

	public void setaMaterno(String aMaterno) {
		this.aMaterno = aMaterno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNombrecompleto() {
		return nombrecompleto;
	}

	public void setNombrecompleto(String nombrecompleto) {
		this.nombrecompleto = nombrecompleto;
	}

	public long getNumempleado() {
		return numempleado;
	}

	public void setNumempleado(long numempleado) {
		this.numempleado = numempleado;
	}

	public String getVip() {
		return vip;
	}

	public void setVip(String vip) {
		this.vip = vip;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getSubdireccion() {
		return subdireccion;
	}

	public void setSubdireccion(String subdireccion) {
		this.subdireccion = subdireccion;
	}

	public long getClavesubdireccion() {
		return clavesubdireccion;
	}

	public void setClavesubdireccion(long clavesubdireccion) {
		this.clavesubdireccion = clavesubdireccion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public long getClavegerencia() {
		return clavegerencia;
	}

	public void setClavegerencia(long clavegerencia) {
		this.clavegerencia = clavegerencia;
	}

	public String getDepto() {
		return depto;
	}

	public void setDepto(String depto) {
		this.depto = depto;
	}

	public long getClavecentrotrabajo() {
		return clavecentrotrabajo;
	}

	public void setClavecentrotrabajo(long clavecentrotrabajo) {
		this.clavecentrotrabajo = clavecentrotrabajo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public long getExt() {
		return ext;
	}

	public void setExt(long ext) {
		this.ext = ext;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public long getCp() {
		return cp;
	}

	public void setCp(long cp) {
		this.cp = cp;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUbicacioncompleta() {
		return ubicacioncompleta;
	}

	public void setUbicacioncompleta(String ubicacioncompleta) {
		this.ubicacioncompleta = ubicacioncompleta;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEdificio() {
		return edificio;
	}

	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAdscripcion() {
		return adscripcion;
	}

	public void setAdscripcion(String adscripcion) {
		this.adscripcion = adscripcion;
	}

	public String getApellidosjefe() {
		return apellidosjefe;
	}

	public void setApellidosjefe(String apellidosjefe) {
		this.apellidosjefe = apellidosjefe;
	}

	public String getNombresjefe() {
		return nombresjefe;
	}

	public void setNombresjefe(String nombresjefe) {
		this.nombresjefe = nombresjefe;
	}

	public String getNombrecompletojefe() {
		return nombrecompletojefe;
	}

	public void setNombrecompletojefe(String nombrecompletojefe) {
		this.nombrecompletojefe = nombrecompletojefe;
	}

	public long getFichajefe() {
		return fichajefe;
	}

	public void setFichajefe(long fichajefe) {
		this.fichajefe = fichajefe;
	}

	public long getExtjefe() {
		return extjefe;
	}

	public void setExtjefe(long extjefe) {
		this.extjefe = extjefe;
	}

	public String getUbicacionjefe() {
		return ubicacionjefe;
	}

	public void setUbicacionjefe(String ubicacionjefe) {
		this.ubicacionjefe = ubicacionjefe;
	}

	public String getNombrejefeinmediato() {
		return nombrejefeinmediato;
	}

	public void setNombrejefeinmediato(String nombrejefeinmediato) {
		this.nombrejefeinmediato = nombrejefeinmediato;
	}

	public String getApellidosresguardo() {
		return apellidosresguardo;
	}

	public void setApellidosresguardo(String apellidosresguardo) {
		this.apellidosresguardo = apellidosresguardo;
	}

	public String getNombresresguardo() {
		return nombresresguardo;
	}

	public void setNombresresguardo(String nombresresguardo) {
		this.nombresresguardo = nombresresguardo;
	}

	public String getNombrecompletoresguardo() {
		return nombrecompletoresguardo;
	}

	public void setNombrecompletoresguardo(String nombrecompletoresguardo) {
		this.nombrecompletoresguardo = nombrecompletoresguardo;
	}

	public String getAdscripcionresguardo() {
		return adscripcionresguardo;
	}

	public void setAdscripcionresguardo(String adscripcionresguardo) {
		this.adscripcionresguardo = adscripcionresguardo;
	}

	public long getExtresguardo() {
		return extresguardo;
	}

	public void setExtresguardo(long extresguardo) {
		this.extresguardo = extresguardo;
	}

	public String getApellidosresponsable() {
		return apellidosresponsable;
	}

	public void setApellidosresponsable(String apellidosresponsable) {
		this.apellidosresponsable = apellidosresponsable;
	}

	public String getNombresresponsable() {
		return nombresresponsable;
	}

	public void setNombresresponsable(String nombresresponsable) {
		this.nombresresponsable = nombresresponsable;
	}

	public String getNombrecompletoresponsable() {
		return nombrecompletoresponsable;
	}

	public void setNombrecompletoresponsable(String nombrecompletoresponsable) {
		this.nombrecompletoresponsable = nombrecompletoresponsable;
	}

	public String getApellidospemex() {
		return apellidospemex;
	}

	public void setApellidospemex(String apellidospemex) {
		this.apellidospemex = apellidospemex;
	}

	public String getNombrespemex() {
		return nombrespemex;
	}

	public void setNombrespemex(String nombrespemex) {
		this.nombrespemex = nombrespemex;
	}

	public String getNombrecompletopemex() {
		return nombrecompletopemex;
	}

	public void setNombrecompletopemex(String nombrecompletopemex) {
		this.nombrecompletopemex = nombrecompletopemex;
	}

	public String getTipoequipo() {
		return tipoequipo;
	}

	public void setTipoequipo(String tipoequipo) {
		this.tipoequipo = tipoequipo;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public String getMarcaequipo() {
		return marcaequipo;
	}

	public void setMarcaequipo(String marcaequipo) {
		this.marcaequipo = marcaequipo;
	}

	public String getModeloequipo() {
		return modeloequipo;
	}

	public void setModeloequipo(String modeloequipo) {
		this.modeloequipo = modeloequipo;
	}

	public String getNumserieequipo() {
		return numserieequipo;
	}

	public void setNumserieequipo(String numserieequipo) {
		this.numserieequipo = numserieequipo;
	}

	public String getEquipocompleto() {
		return equipocompleto;
	}

	public void setEquipocompleto(String equipocompleto) {
		this.equipocompleto = equipocompleto;
	}

	public String getMonitor() {
		return monitor;
	}

	public void setMonitor(String monitor) {
		this.monitor = monitor;
	}

	public String getMarcamonitor() {
		return marcamonitor;
	}

	public void setMarcamonitor(String marcamonitor) {
		this.marcamonitor = marcamonitor;
	}

	public String getModelomonitor() {
		return modelomonitor;
	}

	public void setModelomonitor(String modelomonitor) {
		this.modelomonitor = modelomonitor;
	}

	public String getNumseriemonitor() {
		return numseriemonitor;
	}

	public void setNumseriemonitor(String numseriemonitor) {
		this.numseriemonitor = numseriemonitor;
	}

	public String getMonitorcompleto() {
		return monitorcompleto;
	}

	public void setMonitorcompleto(String monitorcompleto) {
		this.monitorcompleto = monitorcompleto;
	}

	public String getTeclado() {
		return teclado;
	}

	public void setTeclado(String teclado) {
		this.teclado = teclado;
	}

	public String getMarcateclado() {
		return marcateclado;
	}

	public void setMarcateclado(String marcateclado) {
		this.marcateclado = marcateclado;
	}

	public String getModeloteclado() {
		return modeloteclado;
	}

	public void setModeloteclado(String modeloteclado) {
		this.modeloteclado = modeloteclado;
	}

	public String getNumserieteclado() {
		return numserieteclado;
	}

	public void setNumserieteclado(String numserieteclado) {
		this.numserieteclado = numserieteclado;
	}

	public String getTecladocompleto() {
		return tecladocompleto;
	}

	public void setTecladocompleto(String tecladocompleto) {
		this.tecladocompleto = tecladocompleto;
	}

	public String getMouse() {
		return mouse;
	}

	public void setMouse(String mouse) {
		this.mouse = mouse;
	}

	public String getMarcamouse() {
		return marcamouse;
	}

	public void setMarcamouse(String marcamouse) {
		this.marcamouse = marcamouse;
	}

	public String getModelomause() {
		return modelomause;
	}

	public void setModelomause(String modelomause) {
		this.modelomause = modelomause;
	}

	public String getNumseriemouse() {
		return numseriemouse;
	}

	public void setNumseriemouse(String numseriemouse) {
		this.numseriemouse = numseriemouse;
	}

	public String getMousecompleto() {
		return mousecompleto;
	}

	public void setMousecompleto(String mousecompleto) {
		this.mousecompleto = mousecompleto;
	}

	public String getUps() {
		return ups;
	}

	public void setUps(String ups) {
		this.ups = ups;
	}

	public String getMarcaups() {
		return marcaups;
	}

	public void setMarcaups(String marcaups) {
		this.marcaups = marcaups;
	}

	public String getModeloups() {
		return modeloups;
	}

	public void setModeloups(String modeloups) {
		this.modeloups = modeloups;
	}

	public String getNumserieups() {
		return numserieups;
	}

	public void setNumserieups(String numserieups) {
		this.numserieups = numserieups;
	}

	public String getUpscompleto() {
		return upscompleto;
	}

	public void setUpscompleto(String upscompleto) {
		this.upscompleto = upscompleto;
	}

	public String getMaletin() {
		return maletin;
	}

	public void setMaletin(String maletin) {
		this.maletin = maletin;
	}

	public String getMarcamaletin() {
		return marcamaletin;
	}

	public void setMarcamaletin(String marcamaletin) {
		this.marcamaletin = marcamaletin;
	}

	public String getModelomaletin() {
		return modelomaletin;
	}

	public void setModelomaletin(String modelomaletin) {
		this.modelomaletin = modelomaletin;
	}

	public String getNumseriemaletin() {
		return numseriemaletin;
	}

	public void setNumseriemaletin(String numseriemaletin) {
		this.numseriemaletin = numseriemaletin;
	}

	public String getMaletincomleto() {
		return maletincomleto;
	}

	public void setMaletincomleto(String maletincomleto) {
		this.maletincomleto = maletincomleto;
	}

	public String getCandado() {
		return candado;
	}

	public void setCandado(String candado) {
		this.candado = candado;
	}

	public String getMarcacandado() {
		return marcacandado;
	}

	public void setMarcacandado(String marcacandado) {
		this.marcacandado = marcacandado;
	}

	public String getModelocandado() {
		return modelocandado;
	}

	public void setModelocandado(String modelocandado) {
		this.modelocandado = modelocandado;
	}

	public String getNumseriecandado() {
		return numseriecandado;
	}

	public void setNumseriecandado(String numseriecandado) {
		this.numseriecandado = numseriecandado;
	}

	public String getCandadocompleto() {
		return candadocompleto;
	}

	public void setCandadocompleto(String candadocompleto) {
		this.candadocompleto = candadocompleto;
	}

	public String getBocinas() {
		return bocinas;
	}

	public void setBocinas(String bocinas) {
		this.bocinas = bocinas;
	}

	public String getMarcabocinas() {
		return marcabocinas;
	}

	public void setMarcabocinas(String marcabocinas) {
		this.marcabocinas = marcabocinas;
	}

	public String getModelobocinas() {
		return modelobocinas;
	}

	public void setModelobocinas(String modelobocinas) {
		this.modelobocinas = modelobocinas;
	}

	public String getNumseriebocinas() {
		return numseriebocinas;
	}

	public void setNumseriebocinas(String numseriebocinas) {
		this.numseriebocinas = numseriebocinas;
	}

	public String getBocinascompleto() {
		return bocinascompleto;
	}

	public void setBocinascompleto(String bocinascompleto) {
		this.bocinascompleto = bocinascompleto;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara(String camara) {
		this.camara = camara;
	}

	public String getMarcacamara() {
		return marcacamara;
	}

	public void setMarcacamara(String marcacamara) {
		this.marcacamara = marcacamara;
	}

	public String getModelocamara() {
		return modelocamara;
	}

	public void setModelocamara(String modelocamara) {
		this.modelocamara = modelocamara;
	}

	public String getNumseriecmara() {
		return numseriecmara;
	}

	public void setNumseriecmara(String numseriecmara) {
		this.numseriecmara = numseriecmara;
	}

	public String getCamaracompleto() {
		return camaracompleto;
	}

	public void setCamaracompleto(String camaracompleto) {
		this.camaracompleto = camaracompleto;
	}

	public String getMonitor2() {
		return monitor2;
	}

	public void setMonitor2(String monitor2) {
		this.monitor2 = monitor2;
	}

	public String getMarcamonitor2() {
		return marcamonitor2;
	}

	public void setMarcamonitor2(String marcamonitor2) {
		this.marcamonitor2 = marcamonitor2;
	}

	public String getModelomonitor2() {
		return modelomonitor2;
	}

	public void setModelomonitor2(String modelomonitor2) {
		this.modelomonitor2 = modelomonitor2;
	}

	public String getNumseriemonitor2() {
		return numseriemonitor2;
	}

	public void setNumseriemonitor2(String numseriemonitor2) {
		this.numseriemonitor2 = numseriemonitor2;
	}

	public String getMonitor2completo() {
		return monitor2completo;
	}

	public void setMonitor2completo(String monitor2completo) {
		this.monitor2completo = monitor2completo;
	}

	public String getAccesorio() {
		return accesorio;
	}

	public void setAccesorio(String accesorio) {
		this.accesorio = accesorio;
	}

	public String getMarcaaccesorio() {
		return marcaaccesorio;
	}

	public void setMarcaaccesorio(String marcaaccesorio) {
		this.marcaaccesorio = marcaaccesorio;
	}

	public String getModeloaccesorio() {
		return modeloaccesorio;
	}

	public void setModeloaccesorio(String modeloaccesorio) {
		this.modeloaccesorio = modeloaccesorio;
	}

	public String getNumserieaccesorio() {
		return numserieaccesorio;
	}

	public void setNumserieaccesorio(String numserieaccesorio) {
		this.numserieaccesorio = numserieaccesorio;
	}

	public String getAccesoriocompleto() {
		return accesoriocompleto;
	}

	public void setAccesoriocompleto(String accesoriocompleto) {
		this.accesoriocompleto = accesoriocompleto;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getDiscoduro() {
		return discoduro;
	}

	public void setDiscoduro(String discoduro) {
		this.discoduro = discoduro;
	}

	public String getProcesador() {
		return procesador;
	}

	public void setProcesador(String procesador) {
		this.procesador = procesador;
	}

	public String getTipoequipocomp1() {
		return tipoequipocomp1;
	}

	public void setTipoequipocomp1(String tipoequipocomp1) {
		this.tipoequipocomp1 = tipoequipocomp1;
	}

	public String getModelocomp1() {
		return modelocomp1;
	}

	public void setModelocomp1(String modelocomp1) {
		this.modelocomp1 = modelocomp1;
	}

	public String getNumseriecomp1() {
		return numseriecomp1;
	}

	public void setNumseriecomp1(String numseriecomp1) {
		this.numseriecomp1 = numseriecomp1;
	}

	public String getCruceclientecomp1() {
		return cruceclientecomp1;
	}

	public void setCruceclientecomp1(String cruceclientecomp1) {
		this.cruceclientecomp1 = cruceclientecomp1;
	}

	public String getTipoequipocomp2() {
		return tipoequipocomp2;
	}

	public void setTipoequipocomp2(String tipoequipocomp2) {
		this.tipoequipocomp2 = tipoequipocomp2;
	}

	public String getModelocomp2() {
		return modelocomp2;
	}

	public void setModelocomp2(String modelocomp2) {
		this.modelocomp2 = modelocomp2;
	}

	public String getNumseriecomp2() {
		return numseriecomp2;
	}

	public void setNumseriecomp2(String numseriecomp2) {
		this.numseriecomp2 = numseriecomp2;
	}

	public String getCruceclientecomp2() {
		return cruceclientecomp2;
	}

	public void setCruceclientecomp2(String cruceclientecomp2) {
		this.cruceclientecomp2 = cruceclientecomp2;
	}

	public String getTipoequipocomp3() {
		return tipoequipocomp3;
	}

	public void setTipoequipocomp3(String tipoequipocomp3) {
		this.tipoequipocomp3 = tipoequipocomp3;
	}

	public String getModelocomp3() {
		return modelocomp3;
	}

	public void setModelocomp3(String modelocomp3) {
		this.modelocomp3 = modelocomp3;
	}

	public String getNumseriecomp3() {
		return numseriecomp3;
	}

	public void setNumseriecomp3(String numseriecomp3) {
		this.numseriecomp3 = numseriecomp3;
	}

	public String getCruceclientecomp3() {
		return cruceclientecomp3;
	}

	public void setCruceclientecomp3(String cruceclientecomp3) {
		this.cruceclientecomp3 = cruceclientecomp3;
	}

	public String getTipoequipocomp4() {
		return tipoequipocomp4;
	}

	public void setTipoequipocomp4(String tipoequipocomp4) {
		this.tipoequipocomp4 = tipoequipocomp4;
	}

	public String getModelocomp4() {
		return modelocomp4;
	}

	public void setModelocomp4(String modelocomp4) {
		this.modelocomp4 = modelocomp4;
	}

	public String getNumseriecomp4() {
		return numseriecomp4;
	}

	public void setNumseriecomp4(String numseriecomp4) {
		this.numseriecomp4 = numseriecomp4;
	}

	public String getCruceclientecomp4() {
		return cruceclientecomp4;
	}

	public void setCruceclientecomp4(String cruceclientecomp4) {
		this.cruceclientecomp4 = cruceclientecomp4;
	}

	public String getTipoequipocomp5() {
		return tipoequipocomp5;
	}

	public void setTipoequipocomp5(String tipoequipocomp5) {
		this.tipoequipocomp5 = tipoequipocomp5;
	}

	public String getModelocomp5() {
		return modelocomp5;
	}

	public void setModelocomp5(String modelocomp5) {
		this.modelocomp5 = modelocomp5;
	}

	public String getNumseriecomp5() {
		return numseriecomp5;
	}

	public void setNumseriecomp5(String numseriecomp5) {
		this.numseriecomp5 = numseriecomp5;
	}

	public String getCruceclientecomp5() {
		return cruceclientecomp5;
	}

	public void setCruceclientecomp5(String cruceclientecomp5) {
		this.cruceclientecomp5 = cruceclientecomp5;
	}

	public String getTipoequipocomp6() {
		return tipoequipocomp6;
	}

	public void setTipoequipocomp6(String tipoequipocomp6) {
		this.tipoequipocomp6 = tipoequipocomp6;
	}

	public String getModelocomp6() {
		return modelocomp6;
	}

	public void setModelocomp6(String modelocomp6) {
		this.modelocomp6 = modelocomp6;
	}

	public String getNumseriecomp6() {
		return numseriecomp6;
	}

	public void setNumseriecomp6(String numseriecomp6) {
		this.numseriecomp6 = numseriecomp6;
	}

	public String getCruceclientecomp6() {
		return cruceclientecomp6;
	}

	public void setCruceclientecomp6(String cruceclientecomp6) {
		this.cruceclientecomp6 = cruceclientecomp6;
	}

	public String getTipoequipocomp7() {
		return tipoequipocomp7;
	}

	public void setTipoequipocomp7(String tipoequipocomp7) {
		this.tipoequipocomp7 = tipoequipocomp7;
	}

	public String getModelocomp7() {
		return modelocomp7;
	}

	public void setModelocomp7(String modelocomp7) {
		this.modelocomp7 = modelocomp7;
	}

	public String getNumseriecomp7() {
		return numseriecomp7;
	}

	public void setNumseriecomp7(String numseriecomp7) {
		this.numseriecomp7 = numseriecomp7;
	}

	public String getCruceclientecomp7() {
		return cruceclientecomp7;
	}

	public void setCruceclientecomp7(String cruceclientecomp7) {
		this.cruceclientecomp7 = cruceclientecomp7;
	}

	public String getValidacioncomp1() {
		return validacioncomp1;
	}

	public void setValidacioncomp1(String validacioncomp1) {
		this.validacioncomp1 = validacioncomp1;
	}

	public String getValidacioncomp2() {
		return validacioncomp2;
	}

	public void setValidacioncomp2(String validacioncomp2) {
		this.validacioncomp2 = validacioncomp2;
	}

	public String getValidacioncomp3() {
		return validacioncomp3;
	}

	public void setValidacioncomp3(String validacioncomp3) {
		this.validacioncomp3 = validacioncomp3;
	}

	public String getValidacioncomp4() {
		return validacioncomp4;
	}

	public void setValidacioncomp4(String validacioncomp4) {
		this.validacioncomp4 = validacioncomp4;
	}

	public String getValidacioncomp5() {
		return validacioncomp5;
	}

	public void setValidacioncomp5(String validacioncomp5) {
		this.validacioncomp5 = validacioncomp5;
	}

	public String getValidacioncomp6() {
		return validacioncomp6;
	}

	public void setValidacioncomp6(String validacioncomp6) {
		this.validacioncomp6 = validacioncomp6;
	}

	public String getValidacioncomp7() {
		return validacioncomp7;
	}

	public void setValidacioncomp7(String validacioncomp7) {
		this.validacioncomp7 = validacioncomp7;
	}

	public long getValidadoscomp() {
		return validadoscomp;
	}

	public void setValidadoscomp(long validadoscomp) {
		this.validadoscomp = validadoscomp;
	}

	public String getTecniconombre() {
		return tecniconombre;
	}

	public void setTecniconombre(String tecniconombre) {
		this.tecniconombre = tecniconombre;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getReqespecial1() {
		return reqespecial1;
	}

	public void setReqespecial1(String reqespecial1) {
		this.reqespecial1 = reqespecial1;
	}

	public String getReqespecial2() {
		return reqespecial2;
	}

	public void setReqespecial2(String reqespecial2) {
		this.reqespecial2 = reqespecial2;
	}

	public String getObsinv() {
		return obsinv;
	}

	public void setObsinv(String obsinv) {
		this.obsinv = obsinv;
	}

	public String getObsresguardo() {
		return obsresguardo;
	}

	public void setObsresguardo(String obsresguardo) {
		this.obsresguardo = obsresguardo;
	}

	public String getObsextras1() {
		return obsextras1;
	}

	public void setObsextras1(String obsextras1) {
		this.obsextras1 = obsextras1;
	}

	public String getObsextras2() {
		return obsextras2;
	}

	public void setObsextras2(String obsextras2) {
		this.obsextras2 = obsextras2;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFescalacion() {
		return fescalacion;
	}

	public void setFescalacion(String fescalacion) {
		this.fescalacion = fescalacion;
	}

	public String getComentariosescalacion() {
		return comentariosescalacion;
	}

	public void setComentariosescalacion(String comentariosescalacion) {
		this.comentariosescalacion = comentariosescalacion;
	}

	public String getCampolibre1() {
		return campolibre1;
	}

	public void setCampolibre1(String campolibre1) {
		this.campolibre1 = campolibre1;
	}

	public String getCampolibre2() {
		return campolibre2;
	}

	public void setCampolibre2(String campolibre2) {
		this.campolibre2 = campolibre2;
	}

	public String getCampolibre3() {
		return campolibre3;
	}

	public void setCampolibre3(String campolibre3) {
		this.campolibre3 = campolibre3;
	}

	public String getCampolibre4() {
		return campolibre4;
	}

	public void setCampolibre4(String campolibre4) {
		this.campolibre4 = campolibre4;
	}

	public String getCampolibre5() {
		return campolibre5;
	}

	public void setCampolibre5(String campolibre5) {
		this.campolibre5 = campolibre5;
	}

	public String getCampolibre6() {
		return campolibre6;
	}

	public void setCampolibre6(String campolibre6) {
		this.campolibre6 = campolibre6;
	}

	public String getCampolibre7() {
		return campolibre7;
	}

	public void setCampolibre7(String campolibre7) {
		this.campolibre7 = campolibre7;
	}

	public String getCampolibre8() {
		return campolibre8;
	}

	public void setCampolibre8(String campolibre8) {
		this.campolibre8 = campolibre8;
	}

	public String getCampolibre9() {
		return campolibre9;
	}

	public void setCampolibre9(String campolibre9) {
		this.campolibre9 = campolibre9;
	}

	public String getCampolibre10() {
		return campolibre10;
	}

	public void setCampolibre10(String campolibre10) {
		this.campolibre10 = campolibre10;
	}

	public String getCampolibre11() {
		return campolibre11;
	}

	public void setCampolibre11(String campolibre11) {
		this.campolibre11 = campolibre11;
	}

	public String getCampolibre12() {
		return campolibre12;
	}

	public void setCampolibre12(String campolibre12) {
		this.campolibre12 = campolibre12;
	}

	public String getCampolibre13() {
		return campolibre13;
	}

	public void setCampolibre13(String campolibre13) {
		this.campolibre13 = campolibre13;
	}

	public String getCampolibre14() {
		return campolibre14;
	}

	public void setCampolibre14(String campolibre14) {
		this.campolibre14 = campolibre14;
	}

	public String getCampolibre15() {
		return campolibre15;
	}

	public void setCampolibre15(String campolibre15) {
		this.campolibre15 = campolibre15;
	}

	public String getCampolibre16() {
		return campolibre16;
	}

	public void setCampolibre16(String campolibre16) {
		this.campolibre16 = campolibre16;
	}

	public String getCampolibre17() {
		return campolibre17;
	}

	public void setCampolibre17(String campolibre17) {
		this.campolibre17 = campolibre17;
	}

	public String getCampolibre18() {
		return campolibre18;
	}

	public void setCampolibre18(String campolibre18) {
		this.campolibre18 = campolibre18;
	}

	public String getCampolibre19() {
		return campolibre19;
	}

	public void setCampolibre19(String campolibre19) {
		this.campolibre19 = campolibre19;
	}

	public String getCampolibre20() {
		return campolibre20;
	}

	public void setCampolibre20(String campolibre20) {
		this.campolibre20 = campolibre20;
	}

	public String getApellidos2jefe() {
		return apellidos2jefe;
	}

	public void setApellidos2jefe(String apellidos2jefe) {
		this.apellidos2jefe = apellidos2jefe;
	}

	public String getApellidos2resguardo() {
		return apellidos2resguardo;
	}

	public void setApellidos2resguardo(String apellidos2resguardo) {
		this.apellidos2resguardo = apellidos2resguardo;
	}

	public String getApellidos2responsable() {
		return apellidos2responsable;
	}

	public void setApellidos2responsable(String apellidos2responsable) {
		this.apellidos2responsable = apellidos2responsable;
	}

	public String getApellidos2pemex() {
		return apellidos2pemex;
	}

	public void setApellidos2pemex(String apellidos2pemex) {
		this.apellidos2pemex = apellidos2pemex;
	}

	public String getCampoId() {
		return campoId;
	}

	public void setCampoId(String campoId) {
		this.campoId = campoId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Inventario(long inventarioid, long proyectoid, String proyecto, String proyectodescripcion, String fcreacon,
			String folio, long id, String aPaterno, String aMaterno, String nombres, String nombrecompleto,
			long numempleado, String vip, String puesto, String direccion, String subdireccion, long clavesubdireccion,
			String gerencia, long clavegerencia, String depto, long clavecentrotrabajo, String correo, String telefono,
			long ext, String ubicacion, String colonia, long cp, String estado, String ubicacioncompleta, String zona,
			String localidad, String edificio, String piso, String area, String adscripcion, String apellidosjefe,
			String apellidos2jefe, String nombresjefe, String nombrecompletojefe, long fichajefe, long extjefe,
			String ubicacionjefe, String nombrejefeinmediato, String apellidosresguardo, String apellidos2resguardo,
			String nombresresguardo, String nombrecompletoresguardo, String adscripcionresguardo, long extresguardo,
			String apellidosresponsable, String apellidos2responsable, String nombresresponsable,
			String nombrecompletoresponsable, String apellidospemex, String apellidos2pemex, String nombrespemex,
			String nombrecompletopemex, String tipoequipo, String equipo, String marcaequipo, String modeloequipo,
			String numserieequipo, String equipocompleto, String monitor, String marcamonitor, String modelomonitor,
			String numseriemonitor, String monitorcompleto, String teclado, String marcateclado, String modeloteclado,
			String numserieteclado, String tecladocompleto, String mouse, String marcamouse, String modelomause,
			String numseriemouse, String mousecompleto, String ups, String marcaups, String modeloups,
			String numserieups, String upscompleto, String maletin, String marcamaletin, String modelomaletin,
			String numseriemaletin, String maletincomleto, String candado, String marcacandado, String modelocandado,
			String numseriecandado, String candadocompleto, String bocinas, String marcabocinas, String modelobocinas,
			String numseriebocinas, String bocinascompleto, String camara, String marcacamara, String modelocamara,
			String numseriecmara, String camaracompleto, String monitor2, String marcamonitor2, String modelomonitor2,
			String numseriemonitor2, String monitor2completo, String accesorio, String marcaaccesorio,
			String modeloaccesorio, String numserieaccesorio, String accesoriocompleto, String ram, String discoduro,
			String procesador, String tipoequipocomp1, String modelocomp1, String numseriecomp1,
			String cruceclientecomp1, String tipoequipocomp2, String modelocomp2, String numseriecomp2,
			String cruceclientecomp2, String tipoequipocomp3, String modelocomp3, String numseriecomp3,
			String cruceclientecomp3, String tipoequipocomp4, String modelocomp4, String numseriecomp4,
			String cruceclientecomp4, String tipoequipocomp5, String modelocomp5, String numseriecomp5,
			String cruceclientecomp5, String tipoequipocomp6, String modelocomp6, String numseriecomp6,
			String cruceclientecomp6, String tipoequipocomp7, String modelocomp7, String numseriecomp7,
			String cruceclientecomp7, String validacioncomp1, String validacioncomp2, String validacioncomp3,
			String validacioncomp4, String validacioncomp5, String validacioncomp6, String validacioncomp7,
			long validadoscomp, String tecniconombre, String dia, String mes, String anio, String reqespecial1,
			String reqespecial2, String obsinv, String obsresguardo, String obsextras1, String obsextras2,
			String estatus, String fescalacion, String comentariosescalacion, String campolibre1, String campolibre2,
			String campolibre3, String campolibre4, String campolibre5, String campolibre6, String campolibre7,
			String campolibre8, String campolibre9, String campolibre10, String campolibre11, String campolibre12,
			String campolibre13, String campolibre14, String campolibre15, String campolibre16, String campolibre17,
			String campolibre18, String campolibre19, String campolibre20, String campoId) {
		super();
		this.inventarioid = inventarioid;
		this.proyectoid = proyectoid;
		this.proyecto = proyecto;
		this.proyectodescripcion = proyectodescripcion;
		this.fcreacon = fcreacon;
		this.folio = folio;
		this.id = id;
		this.aPaterno = aPaterno;
		this.aMaterno = aMaterno;
		this.nombres = nombres;
		this.nombrecompleto = nombrecompleto;
		this.numempleado = numempleado;
		this.vip = vip;
		this.puesto = puesto;
		this.direccion = direccion;
		this.subdireccion = subdireccion;
		this.clavesubdireccion = clavesubdireccion;
		this.gerencia = gerencia;
		this.clavegerencia = clavegerencia;
		this.depto = depto;
		this.clavecentrotrabajo = clavecentrotrabajo;
		this.correo = correo;
		this.telefono = telefono;
		this.ext = ext;
		this.ubicacion = ubicacion;
		this.colonia = colonia;
		this.cp = cp;
		this.estado = estado;
		this.ubicacioncompleta = ubicacioncompleta;
		this.zona = zona;
		this.localidad = localidad;
		this.edificio = edificio;
		this.piso = piso;
		this.area = area;
		this.adscripcion = adscripcion;
		this.apellidosjefe = apellidosjefe;
		this.apellidos2jefe = apellidos2jefe;
		this.nombresjefe = nombresjefe;
		this.nombrecompletojefe = nombrecompletojefe;
		this.fichajefe = fichajefe;
		this.extjefe = extjefe;
		this.ubicacionjefe = ubicacionjefe;
		this.nombrejefeinmediato = nombrejefeinmediato;
		this.apellidosresguardo = apellidosresguardo;
		this.apellidos2resguardo = apellidos2resguardo;
		this.nombresresguardo = nombresresguardo;
		this.nombrecompletoresguardo = nombrecompletoresguardo;
		this.adscripcionresguardo = adscripcionresguardo;
		this.extresguardo = extresguardo;
		this.apellidosresponsable = apellidosresponsable;
		this.apellidos2responsable = apellidos2responsable;
		this.nombresresponsable = nombresresponsable;
		this.nombrecompletoresponsable = nombrecompletoresponsable;
		this.apellidospemex = apellidospemex;
		this.apellidos2pemex = apellidos2pemex;
		this.nombrespemex = nombrespemex;
		this.nombrecompletopemex = nombrecompletopemex;
		this.tipoequipo = tipoequipo;
		this.equipo = equipo;
		this.marcaequipo = marcaequipo;
		this.modeloequipo = modeloequipo;
		this.numserieequipo = numserieequipo;
		this.equipocompleto = equipocompleto;
		this.monitor = monitor;
		this.marcamonitor = marcamonitor;
		this.modelomonitor = modelomonitor;
		this.numseriemonitor = numseriemonitor;
		this.monitorcompleto = monitorcompleto;
		this.teclado = teclado;
		this.marcateclado = marcateclado;
		this.modeloteclado = modeloteclado;
		this.numserieteclado = numserieteclado;
		this.tecladocompleto = tecladocompleto;
		this.mouse = mouse;
		this.marcamouse = marcamouse;
		this.modelomause = modelomause;
		this.numseriemouse = numseriemouse;
		this.mousecompleto = mousecompleto;
		this.ups = ups;
		this.marcaups = marcaups;
		this.modeloups = modeloups;
		this.numserieups = numserieups;
		this.upscompleto = upscompleto;
		this.maletin = maletin;
		this.marcamaletin = marcamaletin;
		this.modelomaletin = modelomaletin;
		this.numseriemaletin = numseriemaletin;
		this.maletincomleto = maletincomleto;
		this.candado = candado;
		this.marcacandado = marcacandado;
		this.modelocandado = modelocandado;
		this.numseriecandado = numseriecandado;
		this.candadocompleto = candadocompleto;
		this.bocinas = bocinas;
		this.marcabocinas = marcabocinas;
		this.modelobocinas = modelobocinas;
		this.numseriebocinas = numseriebocinas;
		this.bocinascompleto = bocinascompleto;
		this.camara = camara;
		this.marcacamara = marcacamara;
		this.modelocamara = modelocamara;
		this.numseriecmara = numseriecmara;
		this.camaracompleto = camaracompleto;
		this.monitor2 = monitor2;
		this.marcamonitor2 = marcamonitor2;
		this.modelomonitor2 = modelomonitor2;
		this.numseriemonitor2 = numseriemonitor2;
		this.monitor2completo = monitor2completo;
		this.accesorio = accesorio;
		this.marcaaccesorio = marcaaccesorio;
		this.modeloaccesorio = modeloaccesorio;
		this.numserieaccesorio = numserieaccesorio;
		this.accesoriocompleto = accesoriocompleto;
		this.ram = ram;
		this.discoduro = discoduro;
		this.procesador = procesador;
		this.tipoequipocomp1 = tipoequipocomp1;
		this.modelocomp1 = modelocomp1;
		this.numseriecomp1 = numseriecomp1;
		this.cruceclientecomp1 = cruceclientecomp1;
		this.tipoequipocomp2 = tipoequipocomp2;
		this.modelocomp2 = modelocomp2;
		this.numseriecomp2 = numseriecomp2;
		this.cruceclientecomp2 = cruceclientecomp2;
		this.tipoequipocomp3 = tipoequipocomp3;
		this.modelocomp3 = modelocomp3;
		this.numseriecomp3 = numseriecomp3;
		this.cruceclientecomp3 = cruceclientecomp3;
		this.tipoequipocomp4 = tipoequipocomp4;
		this.modelocomp4 = modelocomp4;
		this.numseriecomp4 = numseriecomp4;
		this.cruceclientecomp4 = cruceclientecomp4;
		this.tipoequipocomp5 = tipoequipocomp5;
		this.modelocomp5 = modelocomp5;
		this.numseriecomp5 = numseriecomp5;
		this.cruceclientecomp5 = cruceclientecomp5;
		this.tipoequipocomp6 = tipoequipocomp6;
		this.modelocomp6 = modelocomp6;
		this.numseriecomp6 = numseriecomp6;
		this.cruceclientecomp6 = cruceclientecomp6;
		this.tipoequipocomp7 = tipoequipocomp7;
		this.modelocomp7 = modelocomp7;
		this.numseriecomp7 = numseriecomp7;
		this.cruceclientecomp7 = cruceclientecomp7;
		this.validacioncomp1 = validacioncomp1;
		this.validacioncomp2 = validacioncomp2;
		this.validacioncomp3 = validacioncomp3;
		this.validacioncomp4 = validacioncomp4;
		this.validacioncomp5 = validacioncomp5;
		this.validacioncomp6 = validacioncomp6;
		this.validacioncomp7 = validacioncomp7;
		this.validadoscomp = validadoscomp;
		this.tecniconombre = tecniconombre;
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		this.reqespecial1 = reqespecial1;
		this.reqespecial2 = reqespecial2;
		this.obsinv = obsinv;
		this.obsresguardo = obsresguardo;
		this.obsextras1 = obsextras1;
		this.obsextras2 = obsextras2;
		this.estatus = estatus;
		this.fescalacion = fescalacion;
		this.comentariosescalacion = comentariosescalacion;
		this.campolibre1 = campolibre1;
		this.campolibre2 = campolibre2;
		this.campolibre3 = campolibre3;
		this.campolibre4 = campolibre4;
		this.campolibre5 = campolibre5;
		this.campolibre6 = campolibre6;
		this.campolibre7 = campolibre7;
		this.campolibre8 = campolibre8;
		this.campolibre9 = campolibre9;
		this.campolibre10 = campolibre10;
		this.campolibre11 = campolibre11;
		this.campolibre12 = campolibre12;
		this.campolibre13 = campolibre13;
		this.campolibre14 = campolibre14;
		this.campolibre15 = campolibre15;
		this.campolibre16 = campolibre16;
		this.campolibre17 = campolibre17;
		this.campolibre18 = campolibre18;
		this.campolibre19 = campolibre19;
		this.campolibre20 = campolibre20;
		this.campoId = campoId;
	}

	@Override
	public String toString() {
		return "Inventario [inventarioid=" + inventarioid + ", proyectoid=" + proyectoid + ", proyecto=" + proyecto
				+ ", proyectodescripcion=" + proyectodescripcion + ", fcreacon=" + fcreacon + ", folio=" + folio
				+ ", id=" + id + ", aPaterno=" + aPaterno + ", aMaterno=" + aMaterno + ", nombres=" + nombres
				+ ", nombrecompleto=" + nombrecompleto + ", numempleado=" + numempleado + ", vip=" + vip + ", puesto="
				+ puesto + ", direccion=" + direccion + ", subdireccion=" + subdireccion + ", clavesubdireccion="
				+ clavesubdireccion + ", gerencia=" + gerencia + ", clavegerencia=" + clavegerencia + ", depto=" + depto
				+ ", clavecentrotrabajo=" + clavecentrotrabajo + ", correo=" + correo + ", telefono=" + telefono
				+ ", ext=" + ext + ", ubicacion=" + ubicacion + ", colonia=" + colonia + ", cp=" + cp + ", estado="
				+ estado + ", ubicacioncompleta=" + ubicacioncompleta + ", zona=" + zona + ", localidad=" + localidad
				+ ", edificio=" + edificio + ", piso=" + piso + ", area=" + area + ", adscripcion=" + adscripcion
				+ ", apellidosjefe=" + apellidosjefe + ", apellidos2jefe=" + apellidos2jefe + ", nombresjefe="
				+ nombresjefe + ", nombrecompletojefe=" + nombrecompletojefe + ", fichajefe=" + fichajefe + ", extjefe="
				+ extjefe + ", ubicacionjefe=" + ubicacionjefe + ", nombrejefeinmediato=" + nombrejefeinmediato
				+ ", apellidosresguardo=" + apellidosresguardo + ", apellidosres2guardo=" + apellidos2resguardo
				+ ", nombresresguardo=" + nombresresguardo + ", nombrecompletoresguardo=" + nombrecompletoresguardo
				+ ", adscripcionresguardo=" + adscripcionresguardo + ", extresguardo=" + extresguardo
				+ ", apellidosresponsable=" + apellidosresponsable + ", apellidos2responsable=" + apellidos2responsable
				+ ", nombresresponsable=" + nombresresponsable + ", nombrecompletoresponsable="
				+ nombrecompletoresponsable + ", apellidospemex=" + apellidospemex + ", apellidos2pemex="
				+ apellidos2pemex + ", nombrespemex=" + nombrespemex + ", nombrecompletopemex=" + nombrecompletopemex
				+ ", tipoequipo=" + tipoequipo + ", equipo=" + equipo + ", marcaequipo=" + marcaequipo
				+ ", modeloequipo=" + modeloequipo + ", numserieequipo=" + numserieequipo + ", equipocompleto="
				+ equipocompleto + ", monitor=" + monitor + ", marcamonitor=" + marcamonitor + ", modelomonitor="
				+ modelomonitor + ", numseriemonitor=" + numseriemonitor + ", monitorcompleto=" + monitorcompleto
				+ ", teclado=" + teclado + ", marcateclado=" + marcateclado + ", modeloteclado=" + modeloteclado
				+ ", numserieteclado=" + numserieteclado + ", tecladocompleto=" + tecladocompleto + ", mouse=" + mouse
				+ ", marcamouse=" + marcamouse + ", modelomause=" + modelomause + ", numseriemouse=" + numseriemouse
				+ ", mousecompleto=" + mousecompleto + ", ups=" + ups + ", marcaups=" + marcaups + ", modeloups="
				+ modeloups + ", numserieups=" + numserieups + ", upscompleto=" + upscompleto + ", maletin=" + maletin
				+ ", marcamaletin=" + marcamaletin + ", modelomaletin=" + modelomaletin + ", numseriemaletin="
				+ numseriemaletin + ", maletincomleto=" + maletincomleto + ", candado=" + candado + ", marcacandado="
				+ marcacandado + ", modelocandado=" + modelocandado + ", numseriecandado=" + numseriecandado
				+ ", candadocompleto=" + candadocompleto + ", bocinas=" + bocinas + ", marcabocinas=" + marcabocinas
				+ ", modelobocinas=" + modelobocinas + ", numseriebocinas=" + numseriebocinas + ", bocinascompleto="
				+ bocinascompleto + ", camara=" + camara + ", marcacamara=" + marcacamara + ", modelocamara="
				+ modelocamara + ", numseriecmara=" + numseriecmara + ", camaracompleto=" + camaracompleto
				+ ", monitor2=" + monitor2 + ", marcamonitor2=" + marcamonitor2 + ", modelomonitor2=" + modelomonitor2
				+ ", numseriemonitor2=" + numseriemonitor2 + ", monitor2completo=" + monitor2completo + ", accesorio="
				+ accesorio + ", marcaaccesorio=" + marcaaccesorio + ", modeloaccesorio=" + modeloaccesorio
				+ ", numserieaccesorio=" + numserieaccesorio + ", accesoriocompleto=" + accesoriocompleto + ", ram="
				+ ram + ", discoduro=" + discoduro + ", procesador=" + procesador + ", tipoequipocomp1="
				+ tipoequipocomp1 + ", modelocomp1=" + modelocomp1 + ", numseriecomp1=" + numseriecomp1
				+ ", cruceclientecomp1=" + cruceclientecomp1 + ", tipoequipocomp2=" + tipoequipocomp2 + ", modelocomp2="
				+ modelocomp2 + ", numseriecomp2=" + numseriecomp2 + ", cruceclientecomp2=" + cruceclientecomp2
				+ ", tipoequipocomp3=" + tipoequipocomp3 + ", modelocomp3=" + modelocomp3 + ", numseriecomp3="
				+ numseriecomp3 + ", cruceclientecomp3=" + cruceclientecomp3 + ", tipoequipocomp4=" + tipoequipocomp4
				+ ", modelocomp4=" + modelocomp4 + ", numseriecomp4=" + numseriecomp4 + ", cruceclientecomp4="
				+ cruceclientecomp4 + ", tipoequipocomp5=" + tipoequipocomp5 + ", modelocomp5=" + modelocomp5
				+ ", numseriecomp5=" + numseriecomp5 + ", cruceclientecomp5=" + cruceclientecomp5 + ", tipoequipocomp6="
				+ tipoequipocomp6 + ", modelocomp6=" + modelocomp6 + ", numseriecomp6=" + numseriecomp6
				+ ", cruceclientecomp6=" + cruceclientecomp6 + ", tipoequipocomp7=" + tipoequipocomp7 + ", modelocomp7="
				+ modelocomp7 + ", numseriecomp7=" + numseriecomp7 + ", cruceclientecomp7=" + cruceclientecomp7
				+ ", validacioncomp1=" + validacioncomp1 + ", validacioncomp2=" + validacioncomp2 + ", validacioncomp3="
				+ validacioncomp3 + ", validacioncomp4=" + validacioncomp4 + ", validacioncomp5=" + validacioncomp5
				+ ", validacioncomp6=" + validacioncomp6 + ", validacioncomp7=" + validacioncomp7 + ", validadoscomp="
				+ validadoscomp + ", tecniconombre=" + tecniconombre + ", dia=" + dia + ", mes=" + mes + ", anio="
				+ anio + ", reqespecial1=" + reqespecial1 + ", reqespecial2=" + reqespecial2 + ", obsinv=" + obsinv
				+ ", obsresguardo=" + obsresguardo + ", obsextras1=" + obsextras1 + ", obsextras2=" + obsextras2
				+ ", estatus=" + estatus + ", fescalacion=" + fescalacion + ", comentariosescalacion="
				+ comentariosescalacion + ", campolibre1=" + campolibre1 + ", campolibre2=" + campolibre2
				+ ", campolibre3=" + campolibre3 + ", campolibre4=" + campolibre4 + ", campolibre5=" + campolibre5
				+ ", campolibre6=" + campolibre6 + ", campolibre7=" + campolibre7 + ", campolibre8=" + campolibre8
				+ ", campolibre9=" + campolibre9 + ", campolibre10=" + campolibre10 + ", campolibre11=" + campolibre11
				+ ", campolibre12=" + campolibre12 + ", campolibre13=" + campolibre13 + ", campolibre14=" + campolibre14
				+ ", campolibre15=" + campolibre15 + ", campolibre16=" + campolibre16 + ", campolibre17=" + campolibre17
				+ ", campolibre18=" + campolibre18 + ", campolibre19=" + campolibre19 + ", campolibre20=" + campolibre20
				+ ", campoId=" + campoId + "]";
	}

	

}
