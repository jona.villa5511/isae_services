package com.isae.inventario.modelo;




public class DetalleAgrupador {
	
	public DetalleAgrupador() {
		
	}
	

	
	
	private long enApp;
	
	private String campoNombre;
	private String campoValor;
	private String tipoCampo;
	private String campo;
	private String patter;
	private String mensaje;
	
	
	
	
	
	
	public DetalleAgrupador(long enApp, String campoNombre, String campoValor, String tipoCampo, String campo,String patter, String mensaje) {
		super();
		this.enApp = enApp;
		this.campoNombre = campoNombre;
		this.campoValor = campoValor;
		this.tipoCampo = tipoCampo;
		this.campo = campo;
		this.patter = patter;
		this.mensaje = mensaje;
	}
	public long getEnApp() {
		return enApp;
	}
	public void setEnApp(long enApp) {
		this.enApp = enApp;
	}
	public String getCampoNombre() {
		return campoNombre;
	}
	public void setCampoNombre(String campoNombre) {
		this.campoNombre = campoNombre;
	}
	public String getCampoValor() {
		return campoValor;
	}
	public void setCampoValor(String campoValor) {
		this.campoValor = campoValor;
	}
	

	public String getTipoCampo() {
		return tipoCampo;
	}

	public void setTipoCampo(String tipoCampo) {
		this.tipoCampo = tipoCampo;
	}

	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	
	public String getPatter() {
		return patter;
	}
	public void setPatter(String patter) {
		this.patter = patter;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	@Override
	public String toString() {
		return "DetalleAgrupador [enApp=" + enApp + ", campoNombre=" + campoNombre + ", campoValor=" + campoValor
				+ ", tipoCampo=" + tipoCampo + ", campo=" + campo + "]";
	}
	

	
	
	

	
}