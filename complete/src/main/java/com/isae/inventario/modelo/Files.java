package com.isae.inventario.modelo;


public class Files {
	private Integer id;
	private String fileName;
	private String fileUrl;
	private String fileCoordenadas;
	private Integer idUsuario;
	private String fecha;
	private Integer idInventario;
	private Integer idProyecto;
	private String tipo;
	private int validate;
	public Files() {

	}

	
	public Files(Integer id, String fileName, String fileUrl, String fileCoordenadas, Integer idUsuario,
			String fecha, Integer idInventario, Integer idProyecto, String tipo) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileUrl = fileUrl;
		this.fileCoordenadas = fileCoordenadas;
		this.idUsuario = idUsuario;
		this.fecha = fecha;
		this.idInventario = idInventario;
		this.idProyecto = idProyecto;
		this.tipo=tipo;
	}


	public Files(int validate) {
		super();
		this.validate = validate;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getFileCoordenadas() {
		return fileCoordenadas;
	}

	public void setFileCoordenadas(String fileCoordenadas) {
		this.fileCoordenadas = fileCoordenadas;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getIdInventario() {
		return idInventario;
	}

	public void setIdInventario(Integer idInventario) {
		this.idInventario = idInventario;
	}

	public Integer getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(Integer idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public int getValidate() {
		return validate;
	}


	public void setValidate(int validate) {
		this.validate = validate;
	}


	@Override
	public String toString() {
		return "Files [id=" + id + ", fileName=" + fileName + ", fileUrl=" + fileUrl + ", fileCoordenadas="
				+ fileCoordenadas + ", idUsuario=" + idUsuario + ", fecha=" + fecha + ", idInventario=" + idInventario
				+ ", idProyecto=" + idProyecto + "]";
	}

	

	

	

}
