package com.isae.inventario.modelo;

public class Auxiliar {
	private int id;
	private String descripcion;
	private String colonia;
	private String localidad;
	private String estado;
	private String cp;
	

	public Auxiliar() {

	}

	public Auxiliar(String colonia, String localidad, String estado) {
		super();
		this.colonia = colonia;
		this.localidad = localidad;
		this.estado = estado;
	}

	public Auxiliar(String descripcion) {
		this.descripcion = descripcion;
	}
	

	public Auxiliar(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Auxiliar(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	@Override
	public String toString() {
		return "Auxiliar [id=" + id + ", descripcion=" + descripcion + ", colonia=" + colonia + ", localidad="
				+ localidad + ", estado=" + estado + ", cp=" + cp + "]";
	}

	

	

}
