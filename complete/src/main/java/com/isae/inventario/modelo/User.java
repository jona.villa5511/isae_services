package com.isae.inventario.modelo;

public class User {
	private int usuarioId;
	private String nombreCompleto;
	private String correo;

	private String telefono;
	private String cp;
	private String ubicacion;
	private int perfilId;
	private int jefeId;
	private String usuario;
	private String password;
	private int perfilID;
	private String perfilNombre;
	

	
	
	public User() {

	}



	public int getUsuarioId() {
		return usuarioId;
	}



	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}



	public String getNombreCompleto() {
		return nombreCompleto;
	}



	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}



	public String getCorreo() {
		return correo;
	}



	public void setCorreo(String correo) {
		this.correo = correo;
	}



	public String getTelefono() {
		return telefono;
	}



	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	public String getCp() {
		return cp;
	}



	public void setCp(String cp) {
		this.cp = cp;
	}



	public String getUbicacion() {
		return ubicacion;
	}



	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}



	public int getPerfilId() {
		return perfilId;
	}



	public void setPerfilId(int perfilId) {
		this.perfilId = perfilId;
	}



	public int getJefeId() {
		return jefeId;
	}



	public void setJefeId(int jefeId) {
		this.jefeId = jefeId;
	}



	public String getUsuario() {
		return usuario;
	}



	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public int getPerfilID() {
		return perfilID;
	}



	public void setPerfilID(int perfilID) {
		this.perfilID = perfilID;
	}



	public String getPerfilNombre() {
		return perfilNombre;
	}



	public void setPerfilNombre(String perfilNombre) {
		this.perfilNombre = perfilNombre;
	}



	public User(int usuarioId, String nombreCompleto, int perfilId, int jefeId, String usuario, String password, int perfilID, String perfilNombre) {
		super();
		this.usuarioId = usuarioId;
		this.nombreCompleto = nombreCompleto;
		this.perfilId = perfilId;
		this.jefeId = jefeId;
		this.usuario = usuario;
		this.password = password;
		this.perfilID = perfilID;
		this.perfilNombre = perfilNombre;
	}



	@Override
	public String toString() {
		return "User [usuarioId=" + usuarioId + ", nombreCompleto=" + nombreCompleto + ", correo=" + correo
				+ ", telefono=" + telefono + ", cp=" + cp + ", ubicacion=" + ubicacion + ", perfilId=" + perfilId
				+ ", jefeId=" + jefeId + ", usuario=" + usuario + ", password=" + password + "]";
	}
	
	

}
