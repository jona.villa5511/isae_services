package com.isae.inventario.modelo;

public class Agrupador {
	private int idAgrup;
	private String campo;

	public Agrupador() {

	}

	public int getIdAgrup() {
		return idAgrup;
	}

	public void setIdAgrup(int idAgrup) {
		this.idAgrup = idAgrup;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	

	public Agrupador(int idAgrup, String campo) {
		super();
		this.idAgrup = idAgrup;
		this.campo = campo;
	}

	@Override
	public String toString() {
		return "CamposApp [campo=" + campo + "]";
	}

}
