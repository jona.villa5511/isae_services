package com.isae.inventario.modelo;

public class Proyect {
	private int id;
	private String descripcion;
	private String fecha;

	public Proyect() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Proyect(int id, String descripcion, String fecha) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.fecha = fecha;
	}
	

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Proyect [id=" + id + ", descripcion=" + descripcion + "]";
	}

	
}
