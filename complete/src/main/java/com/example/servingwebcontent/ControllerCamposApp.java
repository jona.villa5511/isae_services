package com.example.servingwebcontent;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.isae.inventario.dao.impl.CamposAgrupadorDaoImpl;
import com.isae.inventario.dao.impl.EmailBody;
import com.isae.inventario.dao.impl.EmailPort;
import com.isae.inventario.dao.impl.EmailService;
import com.isae.inventario.modelo.Agrupador;
import com.isae.inventario.modelo.Auxiliar;
import com.isae.inventario.modelo.DetalleAgrupador;
import com.isae.inventario.modelo.Files;
import com.isae.inventario.modelo.Inventario;
import com.isae.inventario.modelo.Proyect;
import com.isae.inventario.modelo.User;



@RestController
public class ControllerCamposApp {
//	@Autowired
//	private EmailPort emailPort;
	
	
	@CrossOrigin(origins = "*")
	@GetMapping("/inventario/projectsid/{opcion}")
	/*Obtiene los id´s de los proyecto existentes*/
	public List<Proyect> getProyectosId(@PathVariable(value = "opcion")Integer opcion) {
		
CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		
		return Agrup.getProyects(opcion);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("inventario/projects/{id}/{idUsuario}")
	/*Lista los registros del inventario, con el id del proyecto especificado*/
	public List<Inventario> get(@PathVariable(value = "id") Integer id, @PathVariable(value = "idUsuario") Integer idUsuario) {
		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		
		return Agrup.getInventario(id,idUsuario);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/InventBuscar/{id}/{idUsuario}")
	/*Lista los registros del inventario, con el id del proyecto especificado*/
	public List<Inventario> getInventarioBuscar(@RequestBody Inventario insertInven,@PathVariable(value = "id") Integer id, @PathVariable(value = "idUsuario") Integer idUsuario) {
		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		Inventario invent = insertInven;
		return Agrup.getInventarioBuscar(invent.getFolio(),id,idUsuario);
	}
	@CrossOrigin(origins = "*")
	@GetMapping("inventario/maxInventario/{id}")
	/*Lista los registros del inventario, con el id del proyecto especificado*/
	public String getMaxInventario(@PathVariable(value = "id") Integer id) {
		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		
		return Agrup.getMaxInventario(id);
	}
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/ProyectBuscar")
	/*Lista los registros del inventario, con el id del proyecto especificado*/
	public List<Inventario> getInventarioBuscar(@RequestBody Proyect buscaProyect) {
		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		Proyect proyect = buscaProyect;
		return Agrup.getProyectBuscar(proyect.getDescripcion());
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/inventario/projectsAgrup/{idProyec}/{idUsuario}")
	/*Obtiene los id´s de los proyecto existentes*/
	public List<Agrupador> getAgrupador(@PathVariable(value = "idProyec") Integer idProyec,@PathVariable(value = "idUsuario") Integer idUsuario) {
		
		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		
		return Agrup.getAgrupador(idProyec, idUsuario);
	}
	

	@CrossOrigin(origins = "*")
	@GetMapping("inventario/DetailsAgrup/{idProyec}/{idInven}/{idAgrup}/{idUsuario}")
	public List<DetalleAgrupador> getDetailAgrup(@PathVariable(value = "idProyec") Integer idProyec, @PathVariable(value = "idInven") Integer idInven,@PathVariable(value = "idAgrup") Integer idAgrup,@PathVariable(value = "idUsuario") Integer idUsuario) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		System.out.println("paramerrtso URLS::: "+ idProyec + " " + idInven + " " + idAgrup);
		return detalleAgrup.getDetalleAgrupador(idProyec, idInven, idAgrup,idUsuario);
	}
	@CrossOrigin(origins = "*")
	@GetMapping("inventario/DetailsAgrupAdd/{idProyec}/{idAgrup}/{idUsuario}")
	public List<DetalleAgrupador> getDetailAgrupAdd(@PathVariable(value = "idProyec") Integer idProyec,@PathVariable(value = "idAgrup") Integer idAgrup,@PathVariable(value = "idUsuario") Integer idUsuario) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		System.out.println("paramerrtso URLS ADD PIVOT::: "+ idProyec  + " " + idAgrup);
		return detalleAgrup.getDetalleAgrupadorADD(idProyec, idAgrup, idUsuario);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping("inventario/DetailsAgrup/Update/{idProyec}/{idInven}")
	/*Actualziacion de inventqario*/
	public void update(@RequestBody DetalleAgrupador updateInven,@PathVariable(value = "idProyec") Integer idProyec, @PathVariable(value = "idInven") Integer idInven) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		DetalleAgrupador invent = updateInven;
		
		System.out.println("paramerrtso upate:: "+ idProyec + " " + idInven +" "+ invent.getCampoNombre()+" "+ invent.getCampoValor());
		detalleAgrup.updateInventario(idProyec, idInven, invent.getCampoNombre(), invent.getCampoValor());
	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping("user/")
	public List<User> getUserLogin(@RequestBody User user) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		System.out.println("paramerrtso URLS::: "+ user.getUsuario() + " " + user.getPassword());
		return detalleAgrup.getUserLogin(user.getUsuario(), user.getPassword());
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("user/validate")
	public List<User> getUser(@RequestBody User user) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		System.out.println("paramerrtso URLS::: "+ user.getUsuario() );
		return detalleAgrup.getUser(user.getUsuario());
	}
	@CrossOrigin(origins = "*")
	@PostMapping("getuser/correo")
	public List<User> getUserCorreo(@RequestBody User user) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		System.out.println("paramerrtso correo ::: "+ user.getUsuario() );
		return detalleAgrup.getUserCorreo(user.getUsuario());
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping("user/Update/{idUser}")
	/*Actualziacion de usuario*/
	public void update(@RequestBody User userUpdate,@PathVariable(value = "idUser") Integer idUser) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		User user = userUpdate;
		
		System.out.println("user param upate:: "+ idUser + " "+ user.getPassword()+" "+ user.getUsuarioId());
		detalleAgrup.updateUser(idUser,user.getPassword());
	}

	//modifque hoy 19
	@CrossOrigin(origins = "*")
	@GetMapping("inventario/catalogo/{id}")
	public List<Auxiliar> getCatalogos(@PathVariable(value = "id") Integer id) {
		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		return Agrup.getCatalogos(id);
	}
	
	
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/insert/")
	/*Actualziacion de inventqario*/
	public Integer insertInventario(@RequestBody Inventario insertInven) {
		CamposAgrupadorDaoImpl detalleAgrup = new CamposAgrupadorDaoImpl();
		Inventario invent = insertInven;
		
		//System.out.println("paramerrtso insert:: "+ invent.getPiso()+" "+ invent.getNombres());
		return detalleAgrup.insertInventario(invent);
	}

	@CrossOrigin(origins = "*")
	@PostMapping("cp/estado/")
	
	public List<Auxiliar> getEstados() {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getEstado();
	}

	@CrossOrigin(origins = "*")
	@PostMapping("cp/localidad/")
	
	public List<Auxiliar> getLocalidad(@RequestBody Auxiliar localidad) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getLocalidad(localidad.getDescripcion());
	}

	@CrossOrigin(origins = "*")
	@PostMapping("cp/colonia/")
	
	public List<Auxiliar> getColonia(@RequestBody Auxiliar localidad) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getColonia(localidad.getDescripcion());
	}

	@CrossOrigin(origins = "*")
	@PostMapping("cp/")
	
	public List<Auxiliar> getgetAdressCP(@RequestBody Auxiliar cp) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getAdressCP(cp.getCp());
	}
	@CrossOrigin(origins = "*")
	@PostMapping("catalogo/marcaModelo/")
	
	public List<Auxiliar> getcatMarcaModelo(@RequestBody Auxiliar marcaModelo) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getCatMarcaModelo(marcaModelo.getId(),marcaModelo.getDescripcion());
	}
	
	
	@CrossOrigin(origins = "*")
	@PutMapping("inventario/files/insert/")
	
	public void insertFiles(@RequestBody Files files) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		Agrup.insertFiles(files);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/files/consulta/")
	public List<Files> getFiles(@RequestBody Files files) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getFiles(files.getIdProyecto(), files.getIdInventario());
	}
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/filesByNoSerie/consulta/")
	public List<Files> getFilesbyNoSerie(@RequestBody Files files) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();

		return Agrup.getFilesByNumeroSerie(files.getIdProyecto(), files.getIdInventario(), files.getFileName());
	}
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/filesOtros/consulta/")
	public List<Files> getFilesOtros(@RequestBody Files files) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		return Agrup.getFilesValidate(files.getIdProyecto(), files.getIdInventario(), files.getFileName());
	}

	@CrossOrigin(origins = "*")
	@PutMapping("inventario/filesOtros/insert/")
	
	public void insertFilesOtros(@RequestBody Files files) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		Agrup.insertFilesOtros(files);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping("inventario/files/delete/")
	
	public void deleteFiles(@RequestBody Files files) {

		CamposAgrupadorDaoImpl Agrup = new CamposAgrupadorDaoImpl();
		Agrup.deleteFiles(files);
	}

	
	
//	@CrossOrigin(origins = "*")
//	@PostMapping("inventario/correo/")
//	public void SendEmail(@RequestBody EmailBody emailBody)  {
//		System.out.println("entro enviar mail");
//		EmailService emailPort = new EmailService();
//		emailPort.sendEmail(emailBody);
//	}
	@CrossOrigin(origins = "*")
	@PostMapping("inventario/correo/")
	public void sendMail(@RequestBody EmailBody emailBody) {
System.out.println("mail.. " + emailBody.getEmail());
		
//		final String username = "isaedeveloper@gmail.com";
//		final String password = "dymnbobkrsyswgly";
//
//		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
//		props.put("mail.smtp.host", "smtp.gmail.com");
//		props.put("mail.smtp.port", "587");
		
final String username = "soporte@cookysoft.com";
final String password = "s0p0rt3_isa3";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "mail.cookysoft.com");
		props.put("mail.smtp.port", "26");


		try {
			Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
			
			System.out.println(session);

		      // Define message
		      MimeMessage message = new MimeMessage(session);
		      message.setFrom(new InternetAddress(username));
		      message.setSubject(emailBody.getSubject());
		      message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailBody.getEmail()));
		      message.setText(emailBody.getContent());
		      // Envia el mensaje
		      Transport.send(message);
		      System.out.println("Enviado");
		} catch (Exception e) {
			System.out.println("Error ---->");
			System.out.println(e.getMessage());
		}
	}
	
}


